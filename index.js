'use strict';
const through = require('through2');
const read = require('read-file');
const PluginError = require('plugin-error');

const PLUGIN_NAME = 'pirates-caribbean-sea-salt';
var tailwind = __dirname + '/css/tailwind.build.css';
var separator = "\n";


function filesGetContents(filepaths){
    if (!(filepaths instanceof Array)) {
        filepaths = [filepaths];
    }

    const filesContents = [];
    for (var i = 0; i < filepaths.length; i++) {
        filesContents.push(read.sync(filepaths[i], 'utf8'));
    }
    return filesContents;
}

function insert(texts, type) {

    if(!texts){
        throw new PluginError(PLUGIN_NAME, 'Missing text or path !');
    }

    if (!(texts instanceof Array)) {
        texts = [texts];
    }

    if (type !== "append" && type !== "prepend") {
        throw new PluginError(PLUGIN_NAME, 'Missing type !');
    }

    const buffers = [];
    for (var i = 0; i < texts.length; i++) {
        if (type === "prepend") {
            buffers.push(Buffer.from(texts[i].trim() + separator));
        } else if (type === "append") {
            buffers.push(Buffer.from(separator + texts[i].trim()));
        }
    }

    const stream = through.obj(function(file, enc, cb) {
        if (file.isStream()) {
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams are not supported !'));
            return cb();
        }

        if (file.isBuffer()) {
            const concat = [];
            if (type === "append") {
                concat.push(file.contents);
            }
            for (var i = 0; i < buffers.length; i++) {
                concat.push(buffers[i]);
            }
            if (type === "prepend") {
                concat.push(file.contents);
            }

            file.contents = Buffer.concat(concat);
        }

        this.push(file);
        cb();
    });

    return stream;
}

module.exports.aye = function pirates(){
    return insert(filesGetContents(tailwind), "append");  
};

